================================
Guess language with markov chain
================================

Calcule, depuis les textes définis dans :code:`DATASETS`, un `tableau de probabilités`_ d’enchaînement des lettres (chaîne de markov). Puis:

- Calcule la probabilité, suivant ce tableau, d'un enchainement de caractères donné (c-à-d un mot/texte).
- Génère des mots/textes repectant ce tableau.

Soit, si le contenu de :code:`DATASETS` correspond à différentes langues: 

- la possibilité de déterminer dans laquelle de ces langues un texte est écrit
- la possibilité de générer des mots vraissemblables pour ces langues.

Si il est recommandé d'alimenter les modèles avec des datasets les plus généraux possibles : livres (reconnaissance de langue), ou dictionnaires (générateur de mots), on pourra s'essayer à des données plus exotiques, plus restreintes, comme des textes spécifiques à des auteurs, des genres particuliers...

Principe
========

Le programme reprend le principe des chaines de markov dans sa version la plus simple. On prend pour hypothèse que, dans une langue, la propabilité d'apparition d'une lettre dépend directement de celle(s) qui lui précède(nt).  

Il suffit de calculer dans un premier sens : texte => probabilités des enchainements

Puis :

- de calculer dans l'autre sens, et à l'aide de "lancés de dé", de lettres en lettres, générer des mots.
- de calculer la probabilité d'un enchainement particulier (un mot) et de comparer les résultats pour différents modèles

    P\ :sub:`mot`\  =  P\ :sub:`_->m`\  x  P\ :sub:`m->o`\  x  P\ :sub:`o->t`\  x  P\ :sub:`t->_`\

Examples
========

Le script suivant à été entrainé sur des ensembles de textes français, espagnols, anglais et italiens (les modèles, au format json, sont disponibles dans le dossier *modeles*)

Passer simplement, en paramètre du scipt, un fichier contenant un texte dans une langue indéterminée:

.. code:: bash

  python3 calc_markov_chain.py italian_extract.txt

::

    italian_extract.txt :
    ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬ it
    ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬ es : rel diff = 12%
    ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬ fr : rel diff = 21%
    ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬ en : rel diff = 21%

(Les pourcentages sont des valeurs relatives au modèle le plus probable (première ligne), ils constituent une valeur commode et facilement appréhendable mais ne representent rien dans l'absolu. 

Liste des paramètres:
=====================

Vous pouvez modifier quelques paramètres pour controler:

- la création du modèle 
- les sorites/affichages 

Le paramètre le plus intéressant est certainement :code:`MARKOV_CHAIN_MEMORY`, il permet de modifier la taille de la "mémoire" de la chaine, c'est à dire, le nombre de caractère à prendre en compte pour déterminer le suivant (P\ :sub:`x->y`\, P\ :sub:`xy->z`\, P\ :sub:`wxy->z`\  ...)

Les paramètres sont modifiables en entête du fichier ".py". Vous y trouverez des informations compémentaires au tableau ci-dessous.

+------------------------+-------------------------------------------------+----------------------------------+-------------------------------------+
| Paramètre              | Fonction                                        | Valeur                           | Remarques                           |
+------------------------+-------------------------------------------------+----------------------------------+-------------------------------------+
| MARKOV_CHAIN_MEMORY    | taille fenêtre de scan                          | entier >= 2                      | | Ces paramètres influencent        |
|                        |                                                 |                                  | | la précision du modèle,           |
|                        | - 2 => P\ :sub:`x->y`\  (proba y après  x)      |                                  | | mais aussi leur taille            |
|                        | - 3 => P\ :sub:`xy->z`\  (proba z après xy)     |                                  | | en mémoire:                       |
|                        | - 4 => P\ :sub:`wxy->z`\                        |                                  |                                     |
+------------------------+-------------------------------------------------+----------------------------------+ ~ Nb_lettres^\ :sup:`SCAN_LENGTH`\  | 
| IS_ASCII               | transforme le texte en caractères ascii.        | Booléén                          |                                     |
+------------------------+-------------------------------------------------+----------------------------------+ ~ 10ko => 2Mo                       |
| IS_WORD_CHARACTER_OLNY | | retire tout les caractères qui n'appartiennent| Booléen                          |                                     |
|                        | | pas à l'alphabet minuscule :code:`(\W|\d)`    |                                  |                                     |
+------------------------+-------------------------------------------------+----------------------------------+-------------------------------------+
| DATASETS               | liste des modèles d'entrainement                | liste de tuples ::               |                                     | 
|                        |                                                 |                                  |  Chaque ligne correspond à un       |      
|                        |                                                 |    (                             |  modèle, les fichiers peuvent       |  
|                        |                                                 |      (                           |  contenir de la simple liste        |  
|                        |                                                 |        'name' : <id>,            |  de mots à un livre complet         |        
|                        |                                                 |        'model_file' : <fichier>, |  avec ponctuation.                  |       
|                        |                                                 |        'text_type' : <model>     |                                     | 
|                        |                                                 |      ),                          |  id est juste un nom, text_type     |  
|                        |                                                 |      ...                         |  correspond à la façon dont le      |  
|                        |                                                 |    )                             |  texte est traité                   |  
|                        |                                                 |                                  |  voir : paramètres utilisateur      |  
+------------------------+-------------------------------------------------+----------------------------------+-------------------------------------+ 
| IS_GENERATE_WORDS      | Génére des mots suivant le tableau de proba     | Booléén                          |                                     |   
+------------------------+-------------------------------------------------+----------------------------------+                                     | 
| IS_SHOW_TABLE          | Montre les tableaux de probabilités             | Booléén                          |                                     |        
+------------------------+-------------------------------------------------+----------------------------------+-------------------------------------+


.. _tableau de probabilités:

Tableau des probalilités
======================== 

Voici un exemple de tableau pour le français (après translitération vers ascii)

::

    Tableau langue française :
            a  b  c  d  e  f  g  h  i  j  k  l  m  n  o  p  q  r  s  t  u  v  w  x  y  z
    ------------------------------------------------------------------------------------
      | 18  7  2  6  9  8  3  1  1  2  1  0  9  4  2  2  6  3  2  6  3  2  2  0  0  0  0
    a | 19  0  1  2  1  0  0  2  0 23  0  0  4  2 14  0  2  0  8  4  3  7  5  0  0  0  0
    b |  0 15  0  0  0 14  0  0  0 12  0  0 19  0  0 20  0  0 14  1  0  3  0  0  0  0  0
    c |  8  9  0  1  0 22  0  0 19  4  0  0  3  0  0 23  0  0  4  1  2  3  0  0  0  0  0
    d | 12 11  0  0  0 52  0  0  0  9  0  0  0  0  0  5  0  0  3  1  0  6  0  0  0  0  0
    e | 38  1  0  2  0  1  0  1  0  1  0  0  4  3 11  0  1  0  8 14  8  5  1  0  0  0  1
    f |  2 26  0  0  0 13  8  0  0 14  0  0  7  0  0 14  0  0  9  0  0  6  0  0  0  0  0
    g |  2 14  0  0  0 34  0  0  0  4  0  0  4  0  9  6  0  0 15  1  2  9  0  0  0  0  0
    h |  4 31  0  0  0 38  0  0  0  6  0  0  0  0  0 16  0  0  0  0  0  4  0  0  0  0  0
    i |  9  1  1  1  1 14  0  2  0  0  0  0 12  1 10  2  0  1  6 12 22  0  1  0  1  0  0
    j |  8 15  0  0  0 41  0  0  0  0  0  0  0  0  0 26  0  0  0  0  0 10  0  0  0  0  0
    k | 25 10  0  0  0 26  0  0  1 14  0  0  2  2  0  2  0  0  0 12  0  0  0  0  0  5  0
    l | 15 20  0  0  0 36  0  0  0  5  0  0 12  0  0  4  0  1  0  1  0  5  0  0  0  0  0
    m |  2 21  5  0  0 34  0  0  0  8  0  0  0 10  0 12  6  0  0  0  0  2  0  0  0  0  0
    n | 23  4  0  4  6 14  1  3  0  2  0  0  0  0  3  3  0  1  0  9 23  2  1  0  0  0  0
    o |  0  0  1  2  0  1  0  1  0 11  0  0  3  8 25  0  1  0  9  3  3 30  0  0  0  1  0
    p |  2 25  0  0  0 18  0  0  1  5  0  0 10  0  0 17  3  0 12  2  1  4  0  0  0  0  0
    q |  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0 99  0  0  0  0  0
    r | 20 10  0  2  3 29  0  1  0  8  0  0  1  2  1  7  0  0  3  4  4  2  1  0  0  0  0
    s | 53  7  0  1  0 13  0  0  0  4  0  0  0  0  0  7  1  1  0  6  5  3  0  0  0  0  0
    t | 48  8  0  0  0 16  0  0  0  6  0  0  0  0  0  6  0  0  8  2  4  2  0  0  0  0  0
    u | 15  2  1  2  1 11  1  1  0 10  1  0  3  1 11  0  2  0 16  9  8  0  3  0  5  0  0
    v |  0 28  0  0  0 31  0  0  0 15  0  0  0  0  0 20  0  0  5  0  0  1  0  0  0  0  0
    w |  7 43  0  0  0 31  0  0  1  8  0  0  0  0  1  4  0  0  0  0  0  2  0  3  0  0  0
    x | 83  2  0  1  0  3  0  0  0  4  0  0  0  0  0  0  3  0  0  0  2  0  0  0  0  0  0
    y | 35 24  0  0  0 26  0  0  0  0  0  0  1  1  0  3  1  0  2  6  0  0  0  0  0  0  0
    z | 79  3  0  0  0 10  0  0  0  5  0  0  0  0  0  4  0  0  0  0  0  0  0  0  0  0  0

