#!/usr/bin/env python3

import sys
from itertools import islice, chain, count
from collections import deque
import random
import json
import os
import re
from math import log
import operator



##################################################
###################  Constant ####################
##################################################

## text_type

MODEL_TEXT = 1000
# traite les echainements de mots, par exemple, avec MARKOV_CHAIN_MEMORY = 3:
#
#     u-n- -m-o-t
#       |_____|            => comptabilise l'enchainement 'u m' => 'o'
#
# Meilleur support des petites chaines (déterminants) et des liaisons
# => meilleur pour la reconnaissance de textes, totalement inéfficace pour la génération de mots
#
MODEL_WORD = 1001
# Sépare les mots lors de la construction du modèle, par exemple, avec MARKOV_CHAIN_MEMORY = 3:
#
#    u-n- - - -m-o-t
#        |_____|             => compte la probabilité qu'une lettre commence un mot
#                               c-à-d enchainement '   ' => 'm'
#
# Traite les mots indépendament (pour un dataset type dictionnaire)
# => largement meilleur pour génération de mots, moins bon pour la reconnaissance de textes
#
MODEL_BOTH = 1002
# Best of the two worlds:
# Perform a MODEL_TEXT, then get only starting char with MODEL_WORD:
# filter '^\s{2,}\S*$'
#           

##################################################
############  Paramètres utilisateur #############
##################################################

## Caractéristiques de la chaine

# Nombre de caractères utilisés pour prédire le suivant (>=1)
MARKOV_CHAIN_MEMORY = 1
# transliterate texte to ascii. ! surchargé par la valeur du modèle
IS_ASCII = True
# retire tout les caractères ne composant pas de mots, c-à-d qui vérifient (\W|\d)). ! Surchargée par la valeur du modèle
IS_WORD_CHARACTER_ONLY = True

# datasets d'entrainement,: (<nom>, <fichier>, <text_type>)
# Seul le nom est necessaire, text_type=MODEL_BOTH par défaut
DATASETS = (
    {'name' : 'fr', 'model_file' : 'books_fr/all_fr_.txt', 'text_type' : MODEL_BOTH}, # it default, but for example
    {'name' : 'en', 'model_file' : 'books_en/all_en_.txt'},
    {'name' : 'it', 'model_file' : 'books_it/all_it_.txt'},
    {'name' : 'es', 'model_file' : 'books_es/all_es_.txt'},
    {'name' : 'de', 'model_file' : 'books_de/all_de_.txt'},
    {'name' : 'nn', 'model_file' : 'books_nn/all_nn_.txt'},
)
#    {'name' : 'fr', 'model_file' : 'books_fr/all_fr_.txt', 'text_type' : MODEL_TEXT},
#    {'name' : 'fr', 'model_file' : 'books_fr/all_fr_.txt', 'text_type' : MODEL_WORD},
#    ("hugo", "books_fr/hugo_miserabless.txt"),
#    ("zola", "books_fr/zola_assomoir.txt"),
#    ("apollinaire", "books_fr/apollinaire_calligrammes.txt"),
# )


## Paramètres sauvegarde du modèle

MODELs_PATH = "models"
#  indente le fichier json  => taille du fichier plus importante
IS_MODEL_HUMAIN_READABLE = True

# Affichage
IS_GENERATE_WORDS = True  # génère des mots/texte avec le modèle
WORD_START_LETTERS = ''
IS_SHOW_TABLE = True  # montre le tableau de probabilité du modèle


##################################################
###########  Paramètres du programme #############
##################################################

SCAN_LENGTH = MARKOV_CHAIN_MEMORY + 1
SAVE_JSON_INDENT = None if IS_MODEL_HUMAIN_READABLE == False else 4

def set_global_ascii(is_ascii):

    global IS_ASCII
    IS_ASCII = is_ascii

    if IS_ASCII:
        try:
            global unidecode
            from unidecode import unidecode
        except ImportError as e:
            print(
                85*'*', "\nInstallez 'unidecode' pour pouvoir convertir en ascii, ou désactivez l'option IS_ASCII\n", 85*'*'
            )
            raise e

set_global_ascii(IS_ASCII)

def set_global_word_char_only(is_char_only):
    """What a surprising function!!!"""

    global IS_WORD_CHARACTER_ONLY
    IS_WORD_CHARACTER_ONLY = is_char_only

set_global_word_char_only(IS_WORD_CHARACTER_ONLY)

##################################################
#######  Générateur : fenêtres glissantes  #######
##################################################

def sliding_window_over_file(f, size, text_type):
    """Fenêtre glissante sur fichier"""

    init = size * " "
    d = deque(islice(init, size), size)

    for line in f:

        # clean line
        if IS_ASCII:
            line = unidecode(line)
        if IS_WORD_CHARACTER_ONLY:
            line = re.subn("(\W|\d)", " ", line)[0]

        splitter = " " if text_type == MODEL_TEXT or text_type == MODEL_BOTH else " "*(size-1)
        line = re.subn("\s+", splitter, line)[0]

        # add new line when consumed
        init = iter(line.strip().lower())
        for x in init:
            d.append(x)
            yield d


##################################################
########  Class : chaine de markov  ##############
##################################################


class Markov:
    def __init__(self, name, model_file=None, text_type=MODEL_BOTH):

        self.name = name
        self.data_set_file = model_file
        self.text_type = text_type
        self.model_save_file = f"{MODELs_PATH}/{self.name}_markov.json"

        self.dct_proba = self.get_probas()

    def get_probas(self):

        if os.path.exists(self.model_save_file):
            dct_proba = self.load()

        elif self.data_set_file is not None:
            dct_proba = self.populate()

        else:
            print(70*'*',f"\nCan't find any model or data set for '{self.name}'\n", 70*'*')
            raise FileNotFoundError

        return dct_proba

    def populate(self):

        with open(self.data_set_file, "r") as f:

            dct_proba = {}

            for *Lxs, Ly in sliding_window_over_file(f, SCAN_LENGTH, self.text_type):

                Lxs = "".join(Lxs)
                dct_proba.setdefault(Lxs, {})
                dct_proba[Lxs][Ly] = dct_proba[Lxs].setdefault(Ly, 0) + 1

        if self.text_type == MODEL_BOTH: # perform a second scan to get proba for starting letters
            with open(self.data_set_file, "r") as f:

                for *Lxs, Ly in sliding_window_over_file(f, SCAN_LENGTH, MODEL_WORD):

                    # at least 2 spaces starting, and not in between char
                    Lxs = "".join(Lxs)
                    match_txt = re.search('^\s{2,}\S*$', Lxs)
                    if match_txt is not None:
                        dct_proba.setdefault(Lxs, {})
                        dct_proba[Lxs][Ly] = dct_proba[Lxs].setdefault(Ly, 0) + 1

        eps = 5*10**-5 # make probability non zero
        for k in dct_proba.keys():
            dct_proba[k] = {
                a: round(b / sum(dct_proba[k].values()) + eps, 4)
                for a, b in dct_proba[k].items()
            }

        self.save(dct_proba)
        return dct_proba

    def save(self, dct_proba):

        save_obj = {
            "name": self.name,
            "memory-size": MARKOV_CHAIN_MEMORY,
            "ascii": IS_ASCII,
            "alphabet_only": IS_WORD_CHARACTER_ONLY,
            "datas": dct_proba,
        }

        with open(self.model_save_file, "w") as f:
            json.dump(save_obj, f, indent=SAVE_JSON_INDENT)

    def load(self):

        with open(self.model_save_file, "r") as f:
            load_obj = json.load(f)

        dct_proba = load_obj["datas"]

        if load_obj["memory-size"] != MARKOV_CHAIN_MEMORY:
            print(f"""Le paramètre 'MARKOV_CHAIN_MEMORY' ({MARKOV_CHAIN_MEMORY})
                  ne correspond pas à celui du modèle sauvegardé '{self.name}' ({load_obj['memory-size']})""")
            rep = input("Voulez vous écrasez le modèle? [O/n]")

            if not rep or rep[0] in list("YyOo"):
                return self.populate()

        set_global_ascii(load_obj['ascii'])
        set_global_word_char_only(load_obj['alphabet_only'])

        return dct_proba

    def show_table(self):

        dct = self.dct_proba.copy()

        all_keys = set(dct.keys())
        sorted_keys = sorted(all_keys)

        nb_car = len(sorted_keys)
        line_str_format = "{:>3}" * (nb_car + 1)
        line_format = "{:>3}" + "{:3.0f}" * (nb_car)

        print(f"\n{self.name}:")
        print(line_str_format.format(*chain(" ", *sorted_keys)))
        print((nb_car + 1) * 3 * "-")
        for l_x in sorted_keys:
            values = (
                dct[l_x][l_y] if (l_y in dct[l_x].keys()) else 0 for l_y in sorted_keys
            )

            lst = list(chain((l_x + " |",), (v * 100 for v in values)))
            print(line_format.format(*lst))

    def gen_letter(self, letters, weights):

        letter = random.choices(letters, weights=weights)

        return letter[0]

    def create_letters_generator(self, start_letters=''):

        dct = self.dct_proba.copy()

        last_letters = deque(" " * (MARKOV_CHAIN_MEMORY), MARKOV_CHAIN_MEMORY)

        for l in iter(start_letters):
            last_letters.append(l)
            yield l

        while True:

            last_letter_proba = dct.setdefault(''.join(last_letters), {})

            letters = list(last_letter_proba.keys())
            weights = list(last_letter_proba.values())

            if not letters:
                letters, weights = [" "], [1]  # end of word

            last_letter = self.gen_letter(letters, weights)
            last_letters.append(last_letter)

            yield last_letter

    def gen_text(self, length=1000):

        letter_gen = self.create_letters_generator()
        text = ''

        for i in range(length) :
            l = next(letter_gen)
            text += l

        return re.subn('\s+', ' ', text)[0]


    def create_words_generator(self, sz=-1, start_letters=''):

        letter_gen = self.create_letters_generator(start_letters)
        word = ''

        while True:
            l = next(letter_gen)
            if l != ' ':
                word += l
            else: # reset generator
                letter_gen = self.create_letters_generator(start_letters)
                if sz == -1 or len(word) == sz:
                    yield word
                    word = ''

    def gen_word(self, nb, sz=-1, start_letters=''):

        word_gen = self.create_words_generator(sz, start_letters)
        words = []

        for i in range(nb):
            words += [next(word_gen)]

        return words

    def get_word_proba(self, word):

        word = word.strip().lower()

        if IS_ASCII:
            word = unidecode(word)
        if IS_WORD_CHARACTER_ONLY:
            word = re.subn("(\W|\d)", " ", word)[0]

        P = 0  # use logarithm for probas
        dct = self.dct_proba

        for *Lxs, Ly in read_by_(
            (SCAN_LENGTH - 2) * " " + word
        ):  #  start with as space as scan window length

            Lxs = "".join(Lxs)
            dct[Lxs] = dct.setdefault(Lxs, {})
            P = P + log(dct[Lxs].setdefault(Ly, 0.005))  # 1/200 default proba

        return P

    def get_word_proba_file(self, f):

        P = 0  # use logarithm for probas
        dct = self.dct_proba

        for *Lxs, Ly in sliding_window_over_file(f, SCAN_LENGTH, self.text_type):

            Lxs = "".join(Lxs)

            dct[Lxs] = dct.setdefault(Lxs, {})
            P = P + log(dct[Lxs].setdefault(Ly, 0.005))  # 1/200 default proba

        return P


def main():

    markov_list = []
    for model in DATASETS:
        markov_list += [Markov(**model)]

    if IS_SHOW_TABLE:

        for model in markov_list:
            model.show_table()

    if IS_GENERATE_WORDS:
        for model in markov_list:
            print("-----")
            print(model.name)
            #print(re.subn('\s+', ' ', ' '.join(model.gen_word(20, start_letters=WORD_START_LETTERS)))[0])
            print("")
            print(model.gen_text())

    # Get language of file given as parameter
    if len(sys.argv) > 1:

        text = sys.argv[1]

        for model in markov_list:
            f = open(text, "r")
            model.P = model.get_word_proba_file(f)
            f.close()

        markov_list.sort(key=operator.attrgetter("P"))
        markov_list = markov_list[::-1]

        # for model in markov_list:
        #    print(model.name, model.P)

        errs = ""
        for model in markov_list[1:]:
            err = -100 * (markov_list[0].P - model.P) / (markov_list[0].P)
            errs += round((100 - err)) * "▬"
            errs += f" {model.name} : rel diff = {err:.0f}% \n"

        # print("\n----------- text extract -------------------")
        # print(word[: min(150, len(word))])

        print(text, ":")
        print(100 * "▬", f"{markov_list[0].name}")
        print(errs)

    else:
        print("Please give file that you want to guess language as parameter")


if __name__ == "__main__":

    main()
